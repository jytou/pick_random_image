This program chooses a random image in the given directory and shows it full screen.

Parameters: an absolute directory containing jpg files.

The image is chosen for the day, and changes every day at 5am.

Commands that can be used on the focused window:

- arrow right/left: move the image to another screen,
- arrow down/up: switch from full screen to a tiny window on the side of the screen that shows the selected image name,
- Escape: quit the program,
- space: pick another image (not persistent if you restart the program), changes at every press, no cycling,
- backspace: if another image has been selected, reset to default image of the day.
