package pick.random.image;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Main extends JFrame
{
	private static final long serialVersionUID = -6088038253453562904L;

	private static String[] sImageDirs;
	private static int sCurrentDir = 0;

	private static double[] sScaleFactor;
	private static int[] sXOffset;
	private static int[] sYOffset;

	public class MyImagePanel extends JPanel
	{
		private static final long serialVersionUID = -7591014717173867005L;

		@Override
		protected synchronized void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			synchronized (sLock)
			{
				if ((sImage == null) || (sBounds == null))
					return;

				final int originalWidth = sImage.getWidth();
				final int originalHeight = sImage.getHeight();
				final int targetWidth = sBounds.width;
				final int targetHeight = sBounds.height;
				// Calculate the scaling factors for both dimensions
				final double widthScale = (double)targetWidth / originalWidth;
				final double heightScale = (double)targetHeight / originalHeight;
				// Determine the scaling factor that fits the smallest dimension
				final double scaleFactor = sScaleFactor[sCurrentDir] * Math.min(widthScale, heightScale);
				// Calculate the scaled dimensions
				final int scaledWidth = (int)(originalWidth * scaleFactor);
				final int scaledHeight = (int)(originalHeight * scaleFactor);
				// Create a new BufferedImage with the desired dimensions
				final BufferedImage scaledImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
				// Get the graphics object of the scaled image
				final Graphics2D g2d = scaledImage.createGraphics();
				// Set the rendering hint for better quality
				g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				final int x = (targetWidth - scaledWidth) / 2 + sXOffset[sCurrentDir];
				final int y = (targetHeight - scaledHeight) / 2 + sYOffset[sCurrentDir];
				// Draw the scaled image on the new BufferedImage
				g2d.drawImage(sImage, x, y, scaledWidth, scaledHeight, null);
				g2d.dispose();
				g.drawImage(scaledImage, 0, 0, null);
			}
		}
	}

	private static BufferedImage sImage;
	private static Rectangle sBounds;
	private static int sScreenNum = 0;
	private static long sExtraSeed = 0;
	private static boolean sMinimized = false;
	private static String sChosenFile = null;

	private static Date sCurrentDate;
	private static final Object sLock = new Object();

	public Main()
	{
		super("Pick an Image!");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setAlwaysOnTop(true);

		final ImageIcon icon = new ImageIcon(getClass().getResource("/picker.png"));

		if (icon != null)
			setIconImage(icon.getImage());

		getContentPane().setLayout(new BorderLayout());
		if (sMinimized)
		{
			getContentPane().setBackground(Color.black);
			final JLabel label = new JLabel(sChosenFile);
			label.setForeground(Color.lightGray);
			label.setToolTipText(sChosenFile);
			label.addMouseListener(new MouseAdapter()
			{
				@Override
				public void mouseClicked(MouseEvent e)
				{
					super.mouseClicked(e);
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(sChosenFile), null);
				}
			});
			getContentPane().add(label);
		}
		else
			getContentPane().add(new MyImagePanel());

		this.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
				else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
				{
					sScreenNum = Math.min(getScreenDevices().length - 1, sScreenNum + 1);
					SwingUtilities.invokeLater(() -> { selectScreen(Main.this); });
				}
				else if (e.getKeyCode() == KeyEvent.VK_LEFT)
				{
					sScreenNum = Math.max(0, sScreenNum - 1);
					SwingUtilities.invokeLater(() -> { selectScreen(Main.this); });
				}
				else if (sMinimized && (e.getKeyCode() == KeyEvent.VK_UP))
				{
					sMinimized = false;
					pickImageFile();
					SwingUtilities.invokeLater(() -> { selectScreen(Main.this); });
				}
				else if ((!sMinimized) && (e.getKeyCode() == KeyEvent.VK_DOWN))
				{
					sMinimized = true;
					SwingUtilities.invokeLater(() -> { selectScreen(Main.this); });
				}
				else if (e.getKeyCode() == KeyEvent.VK_SPACE)
				{
					sExtraSeed++;
					pickImageFile();
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
				{
					sExtraSeed = 0;
					pickImageFile();
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_PAGE_DOWN)
				{
					sCurrentDir--;
					if (sCurrentDir < 0)
						sCurrentDir = sImageDirs.length - 1;
					sExtraSeed = 0;
					pickImageFile();
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_PAGE_UP)
				{
					sCurrentDir++;
					if (sCurrentDir >= sImageDirs.length)
						sCurrentDir = 0;
					sExtraSeed = 0;
					pickImageFile();
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_ADD)
				{
					sScaleFactor[sCurrentDir] *= 1.1;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_MULTIPLY)
				{
					sScaleFactor[sCurrentDir] *= 2;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_SUBTRACT)
				{
					sScaleFactor[sCurrentDir] /= 1.1;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_DIVIDE)
				{
					sScaleFactor[sCurrentDir] /= 2;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_NUMPAD0)
				{
					sScaleFactor[sCurrentDir] = 1;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_NUMPAD2)
				{
					sYOffset[sCurrentDir] += e.isAltDown() ? 100 : 10;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_NUMPAD8)
				{
					sYOffset[sCurrentDir] -= e.isAltDown() ? 100 : 10;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_NUMPAD4)
				{
					sXOffset[sCurrentDir] -= e.isAltDown() ? 100 : 10;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_NUMPAD6)
				{
					sXOffset[sCurrentDir] += e.isAltDown() ? 100 : 10;
					repaint();
				}
				else if (e.getKeyCode() == KeyEvent.VK_NUMPAD5)
				{
					sXOffset[sCurrentDir] = 0;
					sYOffset[sCurrentDir] = 0;
					repaint();
				}
			}
		});
	}

	private static void selectScreen(final Main pMain)
	{
		synchronized (sLock)
		{
			final GraphicsDevice[] screenDevices = getScreenDevices();
			GraphicsDevice screen = screenDevices[Math.max(0, Math.min(screenDevices.length - 1, sScreenNum))];
			sBounds = screen.getDefaultConfiguration().getBounds();
			SwingUtilities.invokeLater(() ->
			{
				synchronized (sLock)
				{
					if (sMinimized)
					{
						final Main main = new Main();
						main.setBounds(sBounds.x + sBounds.width - 30, sBounds.height * 2 / 3, 30, 50);
						main.setVisible(true);
					}
					else
						screen.setFullScreenWindow(new Main());
					if (pMain != null)
					{
						pMain.setVisible(false);
						pMain.dispose();
						pMain.dispatchEvent(new WindowEvent(pMain, WindowEvent.WINDOW_CLOSING));
					}
				}
			});
		}
	}

	private static GraphicsDevice[] getScreenDevices()
	{
		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final GraphicsDevice[] screenDevices = ge.getScreenDevices();
		return screenDevices;
	}

	private static void pickImageFile()
	{
		try
		{
			final String[] files = Files.walk(Path.of(sImageDirs[sCurrentDir]), FileVisitOption.FOLLOW_LINKS).filter(new Predicate<Path>()
			{
				@Override
				public boolean test(Path path)
				{
					return path.toFile().getName().endsWith(".jpg");
				}
			})
					.map(path -> path.toFile().getAbsolutePath())
					.collect(Collectors.toList())
					.toArray(new String[0]);
			Arrays.sort(files);
			// The image changes at 5am
			final LocalDateTime currentShiftedDate = LocalDateTime.now().minusHours(5);
			final Date date = Date.from(LocalDate.from(currentShiftedDate).atStartOfDay(ZoneId.systemDefault()).toInstant());
			if ((sCurrentDate == null) || (!date.equals(sCurrentDate)))
			// new day, new settings
			{
				sCurrentDate = date;
				Arrays.fill(sScaleFactor, 1);
				Arrays.fill(sXOffset, 0);
				Arrays.fill(sYOffset, 0);
			}
			final Random r = new Random(date.getTime() + sExtraSeed);
			final String chosenFile = files[r.nextInt(files.length)];
			if ((sChosenFile == null) || (!chosenFile.equals(sChosenFile)))
			{
				sChosenFile = chosenFile;
				synchronized (sLock)
				{
					sImage = ImageIO.read(new File(sChosenFile));
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		/*final String[] files = new File(sImageDir).list(new FilenameFilter()
		{
			@Override
			public boolean accept(File pDir, String pName)
			{
				return pName.endsWith(".jpg");
			}
		});*/
	}

	public static void main(String[] args)
	{
		if (args.length > 0)
		{
			sImageDirs = args;
			sScaleFactor = new double[sImageDirs.length];
			Arrays.fill(sScaleFactor, 1);
			sXOffset = new int[sImageDirs.length];
			Arrays.fill(sXOffset, 0);
			sYOffset = new int[sImageDirs.length];
			Arrays.fill(sYOffset, 0);
			pickImageFile();
			selectScreen(null);
		}
		else
			System.err.println("Please specify a directory with images.");
	}
}
